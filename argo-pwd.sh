#!/bin/sh

CLUSTER_NAME="kind-kind"

if [ "$1" != "" ]; then
  CLUSTER_NAME=$1
fi

echo "=== Cluster: $CLUSTER_NAME"

SUFF="--context $CLUSTER_NAME"

IPWD=$(kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" $SUFF)

if [ $? = 0 ];
then
  kubectl -n argocd delete secret argocd-initial-admin-secret $SUFF
  echo "=== Initial password to username \"admin\":"
  echo $IPWD | base64 -d && echo
  echo "=== Login to localhost:8080 and change the admin password
    (argocd login localhost:8080 && argocd accout update-password).
    This shell can be closed afterwards."
  kubectl -n argocd port-forward service/argocd-server 8080:443  $SUFF
else
  echo "=== Either the password has already been changed,
    or the secret has not been created yet (ArgoCD is still in pending)."
fi
