# cluster-setup

### Setup steps:
1. start cluster; i use kind --> `init-kind.sh` script
    - pass a cluster name as an argument; resulting cluster context - kind-$NAME.
      Defaults to kind-kind.
2. setup ArgoCD: `argo-setup.sh <cluster_name>`, where '<cluster_name>' is the cluster context; defaults to `kind-kind`.
    - wait for ArgoCD to finish booting up, then use `argo-pwd.sh <cluster_name>`
      to set password for the "admin" user.
3. run `argo-apps.sh <cluster_name>` to deploy ArgoCD and SealedSecrets via ArgoCD.


---------
### SealedSecrets:
Gets first installed as part of "setup".
kubeseal installation client-side:
```
  mkdir bin
  wget https://github.com/bitnami-labs/sealed-secrets/releases/latest/download/kubeseal-linux-amd64 -O bin/kubeseal
  chmod +x bin/kubeseal
```

The file "ss-master-key.yaml" is the key used to seal/unseal the "test" secrets in the [mt-testing](https://gitlab.cern.ch/aduginet/mt-testing) repo.

To apply (temporary solution?):
```
  kubectl apply -f ss-master-key.yaml --force=true
  kubectl delete pod -n kube-system -l name=sealed-secrets-controller
```

See [here](https://github.com/bitnami-labs/sealed-secrets#how-can-i-do-a-backup-of-my-sealedsecrets) for more info.
