#!/bin/sh

CLUSTER_NAME="kind-kind"

if [ "$1" != "" ]; then
  CLUSTER_NAME=$1
fi

echo "=== Installing ArgoCD to cluster $CLUSTER_NAME"

SUFF="--context $CLUSTER_NAME"

# setup

kubectl create namespace argocd $SUFF
kubectl -n argocd apply -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml $SUFF
kubectl -n argocd patch svc argocd-server -p '{"spec": {"type": "LoadBalancer"}}' $SUFF

# done?

echo "=== When the argocd-server is running, run argo-pwd.sh.
  kubectl -n argocd port-forward service/argocd-server 8080:443
then login to localhost:8080 and change admin password."
