#!/bin/sh

CLUSTER_NAME="kind"

if [ "$1" != "" ]; then
  CLUSTER_NAME=$1
fi

echo "Creating cluster kind-$CLUSTER_NAME."

kind create cluster --name $CLUSTER_NAME

if [ $? == 0 ]; then
  echo "Successful."
else
  echo "Failed!"
fi
