#!/bin/sh

CLUSTER_NAME="kind-kind"

if [ "$1" != "" ]; then
  CLUSTER_NAME=$1
fi

echo "=== Adding apps to Argo CD in cluster $CLUSTER_NAME"

SUFF="--context $CLUSTER_NAME"

# ArgoCD:
echo "- ArgoCD"
argocd app create argo-cd --repo https://gitlab.cern.ch/aduginet/cluster-setup.git --path argo-apps/argo-cd --dest-server https://kubernetes.default.svc --dest-namespace argocd
argocd app sync argo-cd
# Argo (not needed):
# echo "- Argo Workflows"
# argocd app create argo-workflows --repo https://gitlab.cern.ch/aduginet/cluster-setup.git --path argo-apps/argo-workflows --dest-server https://kubernetes.default.svc --dest-namespace argo --sync-option CreateNamespace=true
# argocd app sync argo-workflows
# Sealed Secrets:
echo "- Sealed Secrets"
argocd app create sealedsecrets --repo https://gitlab.cern.ch/aduginet/cluster-setup.git --path argo-apps/argo-cd --dest-server https://kubernetes.default.svc --dest-namespace kube-system
argocd app sync sealedsecrets
