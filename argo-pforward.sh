#!/bin/sh

CONTEXT="kind-kind"

if [ "$1" != "" ]; then
  CONTEXT=$1
fi

kubectl -n argocd port-forward service/argocd-server 8080:443  --context $CONTEXT
